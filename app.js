import express from 'express';
import AWS from 'aws-sdk';

const app = express();
const port = process.env.PORT || 3000;

const endpoint = new AWS.Endpoint(process.env.AWS_URL);
const s3 = new AWS.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	...(process.env.AWS_REGION && { region: process.env.AWS_REGION }),
	endpoint,
	s3ForcePathStyle: true,
	signatureVersion: "v4",
	logger: console
});

app.get('/', (req, res) => {
	res.send('Hello World!');
});

app.get('/s3', async (req, res) => {
	const alwaysPresentBucketName = "zenko-test";

	/* Create bucket */
	try {
		const response = await s3.createBucket({
			Bucket: alwaysPresentBucketName,
			CreateBucketConfiguration: {
				LocationConstraint: process.env.AWS_REGION || "eu-west-1"
			}
		}).promise();
		console.log("Bucket Created Successfully", response.Location);
	}
	catch (error) {
		console.error(`S3 Failed creating bucket: ${error}`);
	}

	/* List buckets */
	try {
		const { Buckets: buckets, Owner: owner } = await s3.listBuckets().promise();
		res.json({
			content: "S3 Buckets",
			endpoint,
			owner,
			buckets
		});
	}
	catch (error) {
		res.send(`S3 Buckets ERROR: ${error}`);
	}
});

app.listen(port, () => {
	console.log(`Example app listening on port ${port}`);
});

export default app;
