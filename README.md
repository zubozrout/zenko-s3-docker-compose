# Docker compose Zenko coudserver s3 test 

Run `docker compose -f zenko-test.yml up` to start the containers.

## Test S3

To test if express.js app is connected to S3 you can try accessing http://localhost:8080/s3

This should list buckets available or an error if connection fails.
